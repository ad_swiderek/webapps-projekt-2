## Pobieranie repozytorium (frontend)

1. W lokalizacji do której chcemy pobrać projekt uruchamiamy Git Bash.
2. W otwartej konsoli wklejamy: git clone https://bitbucket.org/ad_swiderek/webapps-projekt-2.git
3. Nasze repozytorium zostało pobrane.

---

## Pobieranie i uruchamianie backendu 

Backend został pobrany ze strony http://focus-webapps.pl/ -> Ćwiczenia #3 -> backend app

Aby uruchomić naszą aplikację backendową:

1. W wypakowanym folderze z pobraną aplikacją uruchamiamy Git Bash.
2. Wpisujemy komendę npm install
3. Wpisujemy komendę node app.js

Od tej chwili nasza aplikacja backendowa działa o czym zostajemy poinformowani komunikatem app listening on port 3000, 
oczywiście aby wykonywać połączenia należy uzupełnić pola "login" oraz "password" w pliku app.js.

---

## Uruchamianie frontendu i korzystanie z aplikacji

Aby uruchomić naszą aplikację frontendowa, w tle w drugiej konsoli musi działać aplikacja backendowa a następnie:

1. W pobranym folderze z frontendem uruchamiamy Git Bash.
2. Wpisujemy komendę npm install
3. Wpisujemy komendę ng serve
4. Otwieramy przeglądarkę i wpisujemy http://localhost:4200/
5. Gdy pokokaże się nasza aplikacja, w zaznaczonym na czerwono polu wpisujemy numer telefonu i klikamy "Zadzwoń", następnie powinniśmy uzyskać połączenie